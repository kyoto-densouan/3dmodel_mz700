# README #  
  
1/3スケールのMZ-700風小物のstlファイルです。  
スイッチ類、I/Oポート等は省略しています。  
  
組み合わせたファイルとパーツごとに配置したファイルの二種類があります。  
元ファイルはAUTODESK 123D DESIGNです。  


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1982年11月15日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MZ-700)
- [懐かしのホビーパソコン紹介](https://twitter.com/i/events/797642474613141504)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz700/raw/5b1685c76ea1311f5bd6374090422b19dd5399c1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz700/raw/5b1685c76ea1311f5bd6374090422b19dd5399c1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz700/raw/5b1685c76ea1311f5bd6374090422b19dd5399c1/IMG_20181219_093451.jpg)
